<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::post('register', 'UserController@register');
// Route::post('login', 'UserController@login');
// Route::get('book', 'BookController@book');

// Route::get('bookall', 'BookController@bookAuth')->middleware('jwt.verify');
// Route::get('user', 'UserController@getAuthenticatedUser')->middleware('jwt.verify');


Route::group(
  ['middleware' => 'basic.auth'],
  function () {
    Route::group(['prefix' => 'v1'], function () {

      Route::post('register', 'UserController@register');
      Route::post('login', 'UserController@login');
      Route::get('book', 'BookController@book');

      Route::get('bookall', ['middleware' => 'jwt.verify', 'uses' => 'BookController@bookAuth']);
      Route::get('user', ['middleware' => 'jwt.verify', 'uses' => 'UserController@getAuthenticatedUser']);
    });
  }
);
