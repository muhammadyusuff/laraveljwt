<?php

namespace App\Exceptions;

use Log;
use Exception;
use Illuminate\Validation\ValidationException;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Symfony\Component\HttpKernel\Exception\HttpException;

use App\Mail\ExceptionOccured;
use Illuminate\Support\Facades\Mail;
use Symfony\Component\Debug\ExceptionHandler as SymfonyExceptionHandler;
use Symfony\Component\Debug\Exception\FlattenException;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        AuthorizationException::class,
        HttpException::class,
        ModelNotFoundException::class,
        ValidationException::class,
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * @param  \Exception  $exception
     * @return void
     */
    public function report(Exception $exception)
    {
        parent::report($exception);
    }

    public function render($request, Exception $e)
    {
        $rendered = parent::render($request, $e);

        $response = [
            'error' => [
                'code' => $rendered->getStatusCode(),
                'message' => [$e->getMessage()]
            ]
        ];

        if ($e instanceof \Symfony\Component\HttpKernel\Exception\NotFoundHttpException) {
            $response['error']['message'] = ['Not Found'];
        }

        if ($e instanceof \Illuminate\Validation\ValidationException) {
            $response['error']['message'] = [];
            foreach ($e->getResponse()->original as $key => $value) {
                $response['error']['message'][] = $value[0];
            }
        }

        if ($rendered->getStatusCode() === 500) {
            //CHANGE RESPONSE STATUS to 422 when token expired
            if ($response['error']['message'][0] == "Expired token") {
                $response['error']['code'] = 406;
                return response()->json($response, 406);
            }
            Log::error($e->getTraceAsString());
        }

        return response()->json($response, $rendered->getStatusCode());
    }
}
