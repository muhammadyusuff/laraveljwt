<?php

namespace App\Http\Middleware;

use Closure;
use Symfony\Component\HttpKernel\Exception\HttpException;
use \Illuminate\Validation\ValidationException;

class BasicAuth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    protected $API_KEY = 'apikey';
    protected $TOKEN   =  '53fde21c76b0cd3cea5e0d4fcf56df03680697bf14f6e231c1818927808c59e6';

    public function handle($request, Closure $next)
    {
        $authorization = explode(' ', $request->header('authorization'));

        if (empty($this->API_KEY == $authorization[0] && $this->TOKEN == $authorization[1])) {
            throw new ValidationException(null, response()->json([
                'original' => [
                    'Unauthorized'
                ]
            ], 401));
        }

        $request->merge(['basicAuth' => $authorization[1]]);

        return $next($request);
    }
}
