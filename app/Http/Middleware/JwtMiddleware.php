<?php

namespace App\Http\Middleware;

use Closure;
use JWTAuth;
use Exception;
use Tymon\JWTAuth\Http\Middleware\BaseMiddleware;
use Symfony\Component\HttpKernel\Exception\HttpException;
use \Illuminate\Validation\ValidationException;

class JwtMiddleware extends BaseMiddleware
{

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $jwt = $request->header('jwt');

        if (empty($jwt)) {
            throw new ValidationException(null, response()->json([
                'original' => [
                    'Unauthorized'
                ]
            ], 401));
        }

        try {
            $user = JWTAuth::parseToken()->authenticate();
        } catch (Exception $e) {
            if ($e instanceof \Tymon\JWTAuth\Exceptions\TokenInvalidException) {

                throw new ValidationException(null, response()->json([
                    'original' => [
                        'Token is Invalid'
                    ]
                ], 401));
            } else if ($e instanceof \Tymon\JWTAuth\Exceptions\TokenExpiredException) {

                throw new ValidationException(null, response()->json([
                    'original' => [
                        'Token is Expired'
                    ]
                ], 401));
            } else {

                throw new ValidationException(null, response()->json([
                    'original' => [
                        'Authorization Token not found'
                    ]
                ], 401));
            }
        }
        return $next($request);
    }
}
